<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Attempt;

class Termination extends Model
{
	protected $guarded = ['id'];

    public function user()
    {
        return $this->hasOne('App\User')->as('termination_user');
    }

    public function attempts($termination_id)
    {
    	return Attempt::where('termination_id', $termination_id)->get();
    }
}
