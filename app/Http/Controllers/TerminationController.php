<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;

use Auth;
use Cache;
use App\Termination;
use App\Jobs\SendTerminationJob;

class TerminationController extends Controller
{
    protected $redirectTo = '/home';

    /**
     * Save a termination and dispatch fax job
     */
    public function createTermination(Request $request)
    {
        
        // dd( $request->all());

        // Validate: standard + plugin
        $validatedData = $request->validate([
            'phone' => 'required|string|max:255|phone:DE,fixed_line',
            'email' => 'required|string|email|max:255'
        ]);

        $phone = $request->input('phone');

        // remove unnecessary chars
        $phone = str_replace(array('+',' ', '.', '-', '/'), '',  $phone);
        
        // if phone begins with two 00, remove it
        if (strpos($phone, '00') === 0) {
           $phone = substr($phone, 2);
        }

        // if phone begins with 0, remove it
        if (strpos($phone, '0') === 0) {
           $phone = substr($phone, 1);
        }

        // If no country code, add
        if (strpos($phone, '49') !== 0) {
           $phone = '49' . $phone;
        }

        $termination = new Termination([
            'msg' => $request->input('msg'),
            'user_email' => $request->input('email'),
            'provider_phone' =>  $phone,
            'status' =>  'new',
        ]);


        // if user is logged in, save the relation 
        // and remove email to distinguish easily between terminations
        if (Auth::check()) {
            $user = Auth::user();
            $user->terminations()->save($termination);
            $termination->user_email = '';
        }

        $termination->save();

        // Queue sending the fax
        Log::info("Request Cycle with Queues Begins");
        SendTerminationJob::dispatch($termination);
        Log::info("Request Cycle with Queues Ends");

        return redirect('/')->with('status', 'Kündigung nun versendet wurden');
    }



    /**
     * print a list of all terminations for logged in user
     * @return [Array]
     */
    public function listTerminations(){

        if ( !Cache::has(Auth::user()->id . 'terminations') || !Cache::has(Auth::user()->id .'attempts')  ) {
            $terminations = Auth::user()->terminations;
            foreach ($terminations as $key => $termination) {
                $attempts[] = $termination->attempts($termination->id);
            }

            Cache::put(Auth::user()->id .'terminations', $terminations, 5);
            Cache::put(Auth::user()->id .'attempts', $attempts, 5);
        }

        return view('terminations')
                    ->with('terminations', Cache::get(Auth::user()->id .'terminations'))
                    ->with('attempts', Cache::get(Auth::user()->id .'attempts'));
    }

    /**
     * print a list of ALL terminations for all users, visible only to admin
     */
    public function listAllTerminations(){
        if (!Auth::user()->is_admin) {
            return redirect('terminations');
        }

        $terminations = Termination::simplePaginate(25);

         return view('terminationsAll')->with('terminations',  $terminations);

    }
}
