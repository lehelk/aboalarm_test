<?php

namespace App\Jobs;

use App\Termination;
use App\Attempt;
use Log;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendTerminationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $termination;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($termination)
    {
        $this->termination=$termination;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    // Code here to attempt to send the fax
    public function handle()
    {
        $termination = Termination::find($this->termination->id);
        Log::info("Attempt " . $this->attempts() . "to send Termination with ID ". $this->termination->id);
        // Stop trying when 5 attempts reached
        if ($this->attempts() > 5) {
            
            $termination->status = 'failed';

            // delete the job
            $this->delete();
        }
        
        // Randomize success/fail
        if (rand(0, 1)) { 
            // Send successful
            Attempt::create([
                'termination_id' => $this->termination->id,
                'status' => 'done',
            ]);

            $termination->status = 'done';

        }else{
            // Send fail
            Attempt::create([
                'termination_id' => $this->termination->id,
                'status' => 'failed',
            ]);
            $this->fail();
        }

        $termination->save();
    }
}
