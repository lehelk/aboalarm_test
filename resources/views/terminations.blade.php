@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                <p>Termination ID, message, provider phone, status, attempt count</p>
            @foreach ($terminations as $key => $termination)
                <p>{{ $termination->id }}, {{ $termination->msg }}, {{ $termination->provider_phone }}, {{ $termination->status }}, {{ count($attempts[ $key ]) }}</p>
            @endforeach
        </div>
    </div>
</div>
@endsection
