@extends('layouts.app')

@section('content')
            <div class="content">

                <div class="container">                    
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <h3>Register termination</h3>

                        {!! Form::open(['action' => 'TerminationController@createTermination']) !!}
                        
                        <div class="form-group">
                            <label for="">Message:</label>
                            {!! Form::textarea('msg', '', ["class" =>"form-control"] ); !!}
                        </div>
                        <div class="form-group">
                            <label for="">Provider phone:</label>
                            {!! Form::text('phone', '', ["class" =>"form-control"]); !!}
                        </div>
                        <div class="form-group">
                            <label for="">Email:</label>
                            {!! Form::text('email', $email,  ['readonly' => ($email ? 'readonly' : null), "class" =>"form-control"]  ); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Kündigung versenden', ["class" => "btn btn-primary"]); !!}
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
@endsection