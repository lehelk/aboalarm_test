<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

/**
 * Post routes
 */
Route::post('createTermination', 'TerminationController@createTermination');

/**
 * Guarded routes
 */
Route::group(['middleware' => 'auth'], function () {
	Route::get('/meine-kuendigungen', 'TerminationController@listTerminations')->name('terminations');
	Route::get('/alle-kuendigungen', 'TerminationController@listAllTerminations')->name('allterminations');
});