<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('msg')->nullable();
            $table->string('user_email')->nullable();
            $table->string('provider_phone');
            $table->string('status')->nullable();

            $table->timestamps();
        });

        // Pivot table
        Schema::create('termination_user', function (Blueprint $table) {
            $table->integer('termination_id');
            $table->integer('user_id');
            $table->primary(['termination_id' , 'user_id']);
        });

        // 
        Schema::create('attempts', function (Blueprint $table) {
            $table->increments('id');

            // FK
            $table->integer('termination_id')->unsigned();
            $table->foreign('termination_id')->references('id')->on('terminations');
            
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminations');
        Schema::dropIfExists('termination_user');
        Schema::dropIfExists('attempts');
    }
}
