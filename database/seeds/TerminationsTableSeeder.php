<?php

use Illuminate\Database\Seeder;

class TerminationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terminations')->insert([
            'msg' => str_random(10),
            'provider_phone' => str_random(10),
            'status' => 'new'
        ]);

        DB::table('terminations')->insert([
            'msg' => str_random(10),
            'provider_phone' => str_random(10),
            'status' => 'new'
        ]);
        
        DB::table('terminations')->insert([
            'msg' => str_random(10),
            'provider_phone' => str_random(10),
            'status' => 'new'
        ]);

        DB::table('terminations')->insert([
            'msg' => str_random(10),
            'user_email' => str_random(10) . "@gmail.com",
            'provider_phone' => str_random(10),
            'status' => 'new'
        ]);

        DB::table('terminations')->insert([
            'msg' => str_random(10),
            'user_email' => str_random(10) . "@gmail.com",
            'provider_phone' => str_random(10),
            'status' => 'new'
        ]);

        DB::table('terminations')->insert([
            'msg' => str_random(10),
            'user_email' => str_random(10) . "@gmail.com",
            'provider_phone' => str_random(10),
            'status' => 'new'
        ]);

        // Seed the pivot
        DB::table('termination_user')->insert([
            'termination_id' => 1,
            'user_id' => 1
        ]);

        DB::table('termination_user')->insert([
            'termination_id' => 2,
            'user_id' => 1
        ]);

        DB::table('termination_user')->insert([
            'termination_id' => 3,
            'user_id' => 1
        ]);
    }
}
